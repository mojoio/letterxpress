/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@mojoio/letterxpress',
  version: '1.0.15',
  description: 'an unofficial API package for the letterxpress API'
}
